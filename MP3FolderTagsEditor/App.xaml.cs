﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MP3FolderTagsEditor.ViewModel;

namespace MP3FolderTagsEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var view = new MainWindowView
            {
                DataContext = new MainWindowViewModel()
            };


            MainWindowViewModel viewModel = (MainWindowViewModel) view.DataContext;
            view.Closing += viewModel.OnWindowClosing;


            view.Show();
        }
    }
}

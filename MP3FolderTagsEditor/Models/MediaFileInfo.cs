﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP3FolderTagsEditor.Models
{
    internal class MediaFileInfo : INotifyPropertyChanged
    {
        // Selected in datagrid
        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        private bool _checked;
        public bool Checked
        {
            get => _checked;
            set
            {
                // Only changed files can be saved
                _checked = _fileIsChanged && value;
                OnPropertyChanged("Checked");
            }
        }

        private bool _fileIsChanged;
        public bool FileIsChanged
        {
            get => _fileIsChanged;
            set
            {
                _fileIsChanged = value;
                OnPropertyChanged("FileIsChanged");
            } 
        }

        // Full file name
        private string _fullFileName;
        public string FullFileName
        {
            get => _fullFileName;
            set
            {
                _fullFileName = value;
                _fileName = System.IO.Path.GetFileName(_fullFileName);
                OnPropertyChanged("FullFileName");
            }
        }

        // File name without path, for display
        private string _fileName;
        public string FileName
        {
            get => _fileName;
            set
            {
                _fileName = value;
                OnPropertyChanged("FileName");
            }
        }

        #region Old values
        [StringLength(30)]
        private string _oldMediaTitle;
        public string OldMediaTitle
        {
            get => _oldMediaTitle;
            internal set
            {
                _oldMediaTitle = value;
                _newMediaTitle = string.IsNullOrEmpty(_oldMediaTitle) ? Tools.ExtractFileName(_fullFileName) : Tools.ConvertToUtf8(value);
                FileIsChanged = FileIsChanged || String.CompareOrdinal(_oldMediaTitle, _newMediaTitle) != 0;

                OnPropertyChanged("OldMediaTitle");
            }
        }

        [StringLength(30)]
        private string _oldMediaArtist;
        public string OldMediaArtist
        {
            get => _oldMediaArtist;
            internal set
            {
                _oldMediaArtist = value;

                _newMediaArtist = Tools.ConvertToUtf8(value);
                FileIsChanged = FileIsChanged || String.CompareOrdinal(_oldMediaArtist, _newMediaArtist) != 0;

                OnPropertyChanged("OldMediaArtist");
            }
        }

        [StringLength(30)]
        private string _oldMediaAlbum;
        public string OldMediaAlbum
        {
            get => _oldMediaAlbum;
            internal set
            {
                _oldMediaAlbum = value;

                _newMediaAlbum = Tools.ConvertToUtf8(value);
                FileIsChanged = FileIsChanged || String.CompareOrdinal(_oldMediaAlbum, _newMediaAlbum) != 0;

                OnPropertyChanged("OldMediaAlbum");
            }
        }
        #endregion

        #region New values
        [StringLength(30)]
        private string _newMediaTitle;
        public string NewMediaTitle
        {
            get => _newMediaTitle;
            set
            {
                _newMediaTitle = value.Trim();
                OnPropertyChanged("NewMediaTitle");
                FileIsChanged = true;
                Checked = true;
            }
        }

        [StringLength(30)]
        private string _newMediaArtist;
        public string NewMediaArtist
        {
            get => _newMediaArtist;
            set
            {
                _newMediaArtist = value.Trim();
                FileIsChanged = true;
                Checked = true;
                OnPropertyChanged("NewMediaArtist");
            }
        }

        [StringLength(30)]
        private string _newMediaAlbum;
        public string NewMediaAlbum
        {
            get => _newMediaAlbum;
            set
            {
                _newMediaAlbum = value.Trim();
                FileIsChanged = true;
                Checked = true;
                OnPropertyChanged("NewMediaAlbum");
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MP3FolderTagsEditor.Models
{
    internal class MainWindowModel: INotifyPropertyChanged
    {
        public MainWindowModel()
        {
            Caption = "MP3TagsEditor";
        }

        private string _selectedDirectoryName;
        public string SelectedDirectoryName
        {
            get => _selectedDirectoryName;
            set
            {
                _selectedDirectoryName = value;
                OnPropertyChanged("SelectedDirectoryName");
            }
        }

        // Window caption
        public string Caption { get; set; }

        public ObservableCollection<MediaFileInfo> RecordsCollection { get; set; }

        private DataGridHeadersVisibility _gridHeadersVisibility = DataGridHeadersVisibility.None;
        public DataGridHeadersVisibility GridHeadersVisibility
        {
            get => _gridHeadersVisibility;
            set
            {
                _gridHeadersVisibility = value;
                OnPropertyChanged("GridHeadersVisibility");
            }
        }

        #region Enable buttons

        private bool _btnSelectDirectoryEnabled = true;
        public bool BtnSelectDirectoryEnabled
        {
            get => _btnSelectDirectoryEnabled;
            set
            {
                _btnSelectDirectoryEnabled = value;
                OnPropertyChanged("BtnSelectDirectoryEnabled");
            }
        }


        private bool _btnSaveEnabled;
        public bool BtnSaveEnabled
        {
            get => _btnSaveEnabled;
            set
            {
                _btnSaveEnabled = value;
                OnPropertyChanged("BtnSaveEnabled");
            }
        }

        private bool _btnSelectAllEnabled;
        public bool BtnSelectAllEnabled
        {
            get => _btnSelectAllEnabled;
            set
            {
                _btnSelectAllEnabled = value;
                OnPropertyChanged("BtnSelectAllEnabled");
            }
        }

        private bool _btnUnSelectAllEnabled;
        public bool BtnUnSelectAllEnabled
        {
            get => _btnUnSelectAllEnabled;
            set
            {
                _btnUnSelectAllEnabled = value;
                OnPropertyChanged("BtnUnSelectAllEnabled");
            }
        }

        private bool _btnSetTitleFromFileNameEnabled;
        public bool BtnSetTitleFromFileNameEnabled
        {
            get => _btnSetTitleFromFileNameEnabled;
            set
            {
                _btnSetTitleFromFileNameEnabled = value;
                OnPropertyChanged("BtnSetTitleFromFileNameEnabled");
            }
        }

        private Visibility _animationVisible = Visibility.Hidden;
        public Visibility AnimationVisible
        {
            get => _animationVisible;
            set
            {
                _animationVisible = value;
                OnPropertyChanged("AnimationVisible");
            }
        }

        #endregion

        private string _totalFilesCount;
        public string TotalFilesCount
        {
            get => _totalFilesCount;
            set
            {
                _totalFilesCount = value;
                OnPropertyChanged("TotalFilesCount");
            }
        }

        private string _selectedFilesCount;
        public string SelectedFilesCount
        {
            get => _selectedFilesCount;
            set
            {
                _selectedFilesCount = value;
                OnPropertyChanged("SelectedFilesCount");
            }
        }

        private string _labelProcessingFiles;
        public string LabelProcessingFiles
        {
            get => _labelProcessingFiles;
            set
            {
                _labelProcessingFiles = value;
                OnPropertyChanged("LabelProcessingFiles");
            }
        }

        // Current selected item
        private MediaFileInfo _dataGridSelectedItem;
        public MediaFileInfo DataGridSelectedItem
        {
            get => _dataGridSelectedItem;
            set
            {
                _dataGridSelectedItem = value;
                OnPropertyChanged("DataGridSelectedItem");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace MP3FolderTagsEditor
{
    public static class Tools
    {
        private static readonly Dictionary<char, char> CyryllicLetters = new Dictionary<char, char> { { '¸', 'ё' }, { '¹', '№'}, { '³', 'і'} };

        public static string ConvertToUtf8(string s)
        {
            string result = string.IsNullOrEmpty(s) ? s : new string(s.ToCharArray().Select(x => ((x + 848) >= 'А' && (x + 848) <= 'ё') ? (char)(x + 848) : x).ToArray());
            result = string.IsNullOrEmpty(result) ? result : CyryllicLetters.Aggregate(result, (current, item) => current.Replace(item.Key, item.Value));

            return result;
        }

        public static string ExtractFileName(string fullFileName)
        {
            return System.IO.Path.GetFileNameWithoutExtension(fullFileName);
        }
    }
}

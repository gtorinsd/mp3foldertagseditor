﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MahApps.Metro.Controls.Dialogs;
using MP3FolderTagsEditor.Models;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace MP3FolderTagsEditor.ViewModel
{
    internal class MainWindowViewModel
    {
        public MainWindowModel Model { get; set; }

        #region Commands
        public ICommand ClickSelectDirectoryCommand { get; set; }
        public ICommand ClickFilesSaveCommand { get; set; }
        public ICommand ClickSelectAllCommand { get; set; }
        public ICommand ClickUnSelectAllCommand { get; set; }
        public ICommand ClickSetTitleFromFileNameCommand { get; set; }
        public ICommand ClickCloseCommand { get; set; }
        #endregion

        private string _path;
        private MediaFilesWorker _worker;

        private bool _flSaveFile;

        public MainWindowViewModel()
        {
            ClickSelectDirectoryCommand = new Command(arg => ClickSelectDirectoryMethod());
            ClickFilesSaveCommand = new Command(arg => ClickFilesSaveMethodAsync());
            ClickSelectAllCommand = new Command(arg => ClickSelectAllMethod());
            ClickUnSelectAllCommand = new Command(arg => ClickUnSelectAllMethod());
            ClickSetTitleFromFileNameCommand = new Command(arg => ClickSetTitleFromFileNameMethod());
            ClickCloseCommand = new Command(arg => ClickCloseMethod());

            Model = new MainWindowModel
            {
                RecordsCollection = new ObservableCollection<MediaFileInfo>(),
                BtnSelectAllEnabled = false,
                BtnUnSelectAllEnabled = false,
                BtnSetTitleFromFileNameEnabled = false
            };

            ShowFilesCount();
        }

        private void ClickSetTitleFromFileNameMethod()
        {
            Model.DataGridSelectedItem.NewMediaTitle = Tools.ExtractFileName(Model.DataGridSelectedItem.FileName);
            Model.DataGridSelectedItem.Checked = true;
        }

        private void SetGridHeaderVisible(bool isVisible)
        {
            Model.GridHeadersVisibility = isVisible ? DataGridHeadersVisibility.All : DataGridHeadersVisibility.None;
        }

        private async Task SelectDirectory()
        {
            List<MediaFileInfo> list = new List<MediaFileInfo>();
            Cursor previousCursor = Mouse.OverrideCursor;

            try
            {
                var dialog = new CommonOpenFileDialog
                {
                    Title = "Select folder with media files...",
                    IsFolderPicker = true,
                    AddToMostRecentlyUsedList = false,
                    AllowNonFileSystemItems = false,
                    EnsureFileExists = true,
                    EnsurePathExists = true,
                    EnsureReadOnly = false,
                    EnsureValidNames = true,
                    Multiselect = false,
                    ShowPlacesList = true
                };

                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    _path = dialog.FileName;
                    Model.SelectedDirectoryName = _path;
                }

                if (_path == null)
                {
                    return;
                }

                Mouse.OverrideCursor = Cursors.Wait;
                Model.LabelProcessingFiles = "Working with your files...";
                Model.AnimationVisible = Visibility.Visible;
                SetButtonsEnabled(false);
                Model.RecordsCollection.Clear();

                _worker = new MediaFilesWorker();
                string strErrorMsg = string.Empty;
                await Task.Run(
                    () =>
                    {
                        _worker.GetMediaFilesInfo(_path, "*.mp3", ref list, out strErrorMsg);
                    });

                if (!string.IsNullOrEmpty(strErrorMsg))
                {
                    MessageBox.Show(strErrorMsg);
                    return;
                }
            }
            finally
            {
                Mouse.OverrideCursor = previousCursor;
                //Model.AnimationVisible = Visibility.Hidden;
            }

            Model.RecordsCollection.Clear();
            foreach (var item in list)
            {
                item.PropertyChanged += ItemPropertyChanged;
                Model.RecordsCollection.Add(item);
            }

            if (Model.RecordsCollection.Count > 0)
            {
                Model.RecordsCollection[0].IsSelected = true;
            }

            Model.AnimationVisible = Visibility.Hidden;
            SetButtonsEnabled();

            SetGridHeaderVisible(Model.RecordsCollection.Count > 0);
            if (Model.RecordsCollection.Count == 0)
            {
                MessageBox.Show("No audio files found");
            }

            int i = list.Count;
            Model.BtnSelectAllEnabled = i > 0;
            Model.BtnUnSelectAllEnabled = i > 0;
            Model.BtnSetTitleFromFileNameEnabled = i > 0;

            ShowFilesCount();
        }

        private void ShowFilesCount()
        {
            int i = Model.RecordsCollection.Count(item => item.Checked);
            Model.SelectedFilesCount = $"Selected files: {i}";

            if (!_flSaveFile)
            {
                Model.BtnSaveEnabled = i > 0;
            }

            Model.TotalFilesCount = $"Total files: {Model.RecordsCollection.Count}";
        }

        private void SetButtonsEnabled(bool enabled = true)
        {
            Model.BtnSaveEnabled = enabled;
            Model.BtnSelectAllEnabled = enabled;
            Model.BtnUnSelectAllEnabled = enabled;
            Model.BtnSetTitleFromFileNameEnabled = enabled;
            Model.BtnSelectDirectoryEnabled = enabled;
        }

        private void SelectAll(bool flag = true)
        {
            foreach (var item in Model.RecordsCollection)
            {
                if (item.FileIsChanged)
                {
                    item.Checked = flag;
                }

                // Unselect all files
                if (!flag)
                {
                    item.Checked = false;
                }
            }
            ShowFilesCount();
        }

        #region Event handlers
        private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Checked")
            {
                ShowFilesCount();
            }
        }

        private async void ClickSelectDirectoryMethod()
        {
            await SelectDirectory();
        }

        private async void ClickFilesSaveMethodAsync()
        {
            int i = -1;
            string strErrorMsg = string.Empty;
            Cursor previousCursor = Mouse.OverrideCursor;
            Mouse.OverrideCursor = Cursors.Wait;
            Model.LabelProcessingFiles = "Saving your files...";
            Model.AnimationVisible = Visibility.Visible;
            _flSaveFile = true;
            SetButtonsEnabled(false);

            try
            {
                await Task.Run(() =>
                    {
                        i = _worker.SaveMediaFilesInfo(Model.RecordsCollection, out strErrorMsg);
                    });
            }
            finally
            {
                Mouse.OverrideCursor = previousCursor;
                Model.AnimationVisible = Visibility.Hidden;
                _flSaveFile = false;
                SetButtonsEnabled();
            }

            if (i >= 0)
            {
                MessageBox.Show($"Saved {i} files");
            }
            else
            {
                MessageBox.Show(strErrorMsg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClickSelectAllMethod()
        {
            SelectAll();
        }

        private void ClickUnSelectAllMethod()
        {
            SelectAll(false);
        }

        private void ClickCloseMethod()
        {
            if (Application.Current.MainWindow != null) Application.Current.MainWindow.Close();
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (Model.RecordsCollection.Count(item => item.FileIsChanged) > 0)
            {
                var result =
                    MessageBox.Show("There are modified and unsaved files.\nAre you sure do you want to close?",
                        Model.Caption, MessageBoxButton.OKCancel, MessageBoxImage.Asterisk);
                if (result != MessageBoxResult.OK)
                {
                    e.Cancel = true;
                }
            }
        }

        #endregion
    }
}

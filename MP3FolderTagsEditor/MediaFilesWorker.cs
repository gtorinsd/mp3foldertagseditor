﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MP3FolderTagsEditor.Models;

namespace MP3FolderTagsEditor
{
    internal class MediaFilesWorker
    {
        public bool GetMediaFilesInfo(string path, string fileMask, ref List<MediaFileInfo> list, out string strErrorMsg)
        {
            strErrorMsg = string.Empty;
            // Process Files
            foreach (var file in Directory.GetFiles(path, fileMask))
            {

                var item = new MediaFileInfo {FullFileName = file};
                try
                {
                    using (var mediaFile = TagLib.File.Create(file))
                    {
                        // Title
                        item.OldMediaTitle = mediaFile.Tag.Title;

                        // Artist
                        string[] artists = mediaFile.Tag.Performers;
                        if (artists.Length > 0)
                        {
                            item.OldMediaArtist = artists[0];
                        }

                        // Album
                        item.OldMediaAlbum = mediaFile.Tag.Album;
                    }
                    list.Add(item);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine($"Error: {item.FullFileName}\n{ex.Message}");
                    strErrorMsg = ex.Message;
                }
            }

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(path);
            foreach (string subdirectory in subdirectoryEntries)
            {
                GetMediaFilesInfo(subdirectory, fileMask, ref list, out strErrorMsg);
            }

            return true;
        }

        public int SaveMediaFilesInfo(ObservableCollection<MediaFileInfo> list, out string strErrorMsg)
        {
            strErrorMsg = string.Empty;
            int result = 0;
            try
            {
                foreach (var item in list)
                {
                    if (!item.Checked || !item.FileIsChanged)
                    {
                        continue;
                    }

                    using (var mediaFile = TagLib.File.Create(item.FullFileName))
                    {
                        mediaFile.Tag.Title = item.NewMediaTitle;
                        mediaFile.Tag.AlbumArtists = new[] {item.NewMediaArtist};
                        mediaFile.Tag.Performers = mediaFile.Tag.AlbumArtists;
                        mediaFile.Tag.Album = item.NewMediaAlbum;

                        // Show updated fields in DataGrid
                        item.OldMediaTitle = item.NewMediaTitle;
                        item.OldMediaArtist = item.NewMediaArtist;
                        item.OldMediaAlbum = item.NewMediaAlbum;

                        item.FileIsChanged = false;
                        item.Checked = false;

                        mediaFile.Save();
                        result++;
                    }
                }
            }
            catch (Exception ex)
            {
                strErrorMsg = ex.Message;
                return -1;
            }
            return result;
        }
    }
}
